variable "prefix" {
  default = "raad" #recipe-app-api-devops
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "ahmadali.gu@gmail.com"
}

variable "db_username" {
  description = "Username for RDS Postgres Instance"
}

variable "db_password" {
  description = "Password for RDS Postgres Instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}
