
#Terraform S3 Backend Block Below:-
terraform {
  backend "s3" {
    bucket         = "shazad-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

#Locking Provider Version for stability of this project
provider "aws" {
  region  = "us-east-1"
  version = "~>2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}" #eg. raad-dev, raad-prod
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

#Retrieve Current Region
data "aws_region" "current" {}
