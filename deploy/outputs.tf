#Output of Certain variable or Attributes on one of our Resources
output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}
